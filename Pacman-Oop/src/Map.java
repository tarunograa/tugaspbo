import java.awt.*;
import java.awt.image.BufferedImage;
public class Map extends Entities{
    private int BLOCK_SIZE = 24;
    private int N_BLOCKS = 15;
    private final int SCREEN_SIZE = N_BLOCKS * BLOCK_SIZE;
    private int[] screenData = new int[N_BLOCKS * N_BLOCKS];;
    private final Font smallFont = new Font("Arial", Font.BOLD, 14);

    public int getBLOCK_SIZE() {
        return this.BLOCK_SIZE;
    }

    public int getN_BLOCKS() {
        return this.N_BLOCKS;
    }

    public int[] getScreenData() {
        return this.screenData;
    }

    public void setScreenData(int index, int value){
        this.screenData[index] = value;
    }

    
    //Level data adalah penyusun dari map pada game
    //0 berarti dot atau titik akan berwarna biru
    //1 Border kiri
    //2 top border
    //4 border kanan
    //8 Border bawah
    //16 titik putih
    //Sebagai contoh 19, 19  merepresentasikan titik di kiri atas map, yang berarti penyusunnya adalah 
    //border kiri (1), border atas (2) dan titik putih (16), sehingga apabila dijumlahkan total = 19
    private final int levelData[] = {
        19, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 22,
        17, 16, 16, 16, 16, 24, 16, 16, 16, 16, 16, 16, 16, 16, 20,
        25, 24, 24, 24, 28, 0, 17, 16, 16, 16, 16, 16, 16, 16, 20,
        0, 0, 0, 0, 0, 0, 17, 16, 16, 16, 16, 16, 16, 16, 20,
        19, 18, 18, 18, 18, 18, 16, 16, 16, 16, 24, 24, 24, 24, 20,
        17, 16, 16, 16, 16, 16, 16, 16, 16, 20, 0, 0, 0, 0, 21,
        17, 16, 16, 16, 16, 16, 16, 16, 16, 20, 0, 0, 0, 0, 21,
        17, 16, 16, 16, 24, 16, 16, 16, 16, 20, 0, 0, 0, 0, 21,
        17, 16, 16, 20, 0, 17, 16, 16, 16, 16, 18, 18, 18, 18, 20,
        17, 24, 24, 28, 0, 25, 24, 24, 16, 16, 16, 16, 16, 16, 20,
        21, 0, 0, 0, 0, 0, 0, 0, 17, 16, 16, 16, 16, 16, 20,
        17, 18, 18, 22, 0, 19, 18, 18, 16, 16, 16, 16, 16, 16, 20,
        17, 16, 16, 20, 0, 17, 16, 16, 16, 16, 16, 16, 16, 16, 20,
        17, 16, 16, 20, 0, 17, 16, 16, 16, 16, 16, 16, 16, 16, 20,
        25, 24, 24, 24, 26, 24, 24, 24, 24, 24, 24, 24, 24, 24, 28
    };

    public int[] getLevelData(){
        return this.levelData;
    }

    public void showIntroScreen(Graphics2D g2d) {
        String start = "Press SPACE to start";
        g2d.setColor(Color.yellow);
        g2d.drawString(start, (SCREEN_SIZE) / 4, 150);
    }
    public void drawScore(Graphics2D g, int score, BufferedImage heart, int lives) {
 
        g.setFont(smallFont);
        g.setColor(new Color(5, 181, 79));
        String s = "High Score : "+Entities.highsScore+", Score: " + score;
        // System.out.println("fdskfj");
        g.drawString(s, SCREEN_SIZE / 2 + -10, SCREEN_SIZE + 16);

        for (int i = 0; i < lives; i++) {
            g.drawImage(heart, i * 28 + 8, SCREEN_SIZE + 1, this);
        }
    }
    

    public void checkMaze(Ghost ghost, Pacman pacman){
        int i = 0;
        boolean finished = true;
        while (i < N_BLOCKS * N_BLOCKS && finished) {
            if ((screenData[i] & 48 ) != 0) {
                finished = false;
            }
            i++;
        }
        if (finished) {
            pacman.setScore(pacman.getScore()+50);
            ghost.setN_GHOSTS(ghost.getN_GHOSTS()+2);
            if (ghost.getN_GHOSTS() < ghost.getMAX_GHOSTS()) {
                ghost.setN_GHOSTS(ghost.getN_GHOSTS()+1);
            }
            if (getCurrentSpeed() < getMaxSpeed()) {
                setCurrentSpeed(getCurrentSpeed()+1);
            }
            pacman.initLevel(ghost, this);
        }
    }

    //Fungsi untuk menggambar map dari pacman
    public void drawMaze(Graphics2D g2d) {

        int i = 0;
        int x, y;

        for (y = 0; y < SCREEN_SIZE; y += BLOCK_SIZE) {
            for (x = 0; x < SCREEN_SIZE; x += BLOCK_SIZE) {

                g2d.setColor(new Color(0, 72, 251));//set ke warna biru
                g2d.setStroke(new BasicStroke(5));

                //menggambar obstacle di tengah map
                //Jika ditemukan nilai 0 maka akan 
                if ((levelData[i] == 0)) {
                    g2d.fillRect(x, y, BLOCK_SIZE, BLOCK_SIZE);
                }

                //Draw line untuk left border
                if ((screenData[i] & 1) != 0) {
                    g2d.drawLine(x, y, x, y + BLOCK_SIZE - 1);
                }

                //Draw line untuk top border
                if ((screenData[i] & 2) != 0) {
                    g2d.drawLine(x, y, x + BLOCK_SIZE - 1, y);
                }
                //Right border
                if ((screenData[i] & 4) != 0) {
                    g2d.drawLine(x + BLOCK_SIZE - 1, y, x + BLOCK_SIZE - 1, y + BLOCK_SIZE - 1);
                }

                //Bottom border
                if ((screenData[i] & 8) != 0) {
                    g2d.drawLine(x, y + BLOCK_SIZE - 1, x + BLOCK_SIZE - 1, y + BLOCK_SIZE - 1);
                }
                //Draw dot/titik pada map
                if ((screenData[i] & 16) != 0) {
                    //set warna ke putih
                    g2d.setColor(new Color(255, 255, 255));
                    g2d.fillOval(x + 10, y + 10, 6, 6);
                }

                i++;
            }
        }
    }
}
