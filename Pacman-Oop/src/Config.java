import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

//Kelas Abstract        
abstract class Config {
    //Mengkoneksikan ke database
    static final String DB_URL = "jdbc:sqlite:HighScore.db";
    //Deklarasi variabel yang diperlukan
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;
    public Config() {
        try {
            conn = DriverManager.getConnection(DB_URL);
            stmt = conn.createStatement();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //Method abstract
    public abstract void execute(String data);
}
