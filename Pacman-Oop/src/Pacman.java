import java.awt.*;
import java.awt.image.BufferedImage;

public class Pacman extends Entities implements Runnable {

    private boolean dying = false;
    private final int PACMAN_SPEED = 6; // kecepatan pacman
    private int lives;
    private int score;
    private BufferedImage up, down, left, right;
    private BufferedImage heart;
    private int pacman_x, pacman_y;
    private int pacmand_x, pacmand_y;
    private int req_dx, req_dy;

    public Pacman() {
        gambar();
    }

    public void initGame(Ghost ghost, Map map) {
        lives = 3;
        System.out.println("didint go here");
        score = 0;
        initLevel(ghost, map);
        ghost.setN_GHOSTS(6);
        setCurrentSpeed(3);
    }

    public void initLevel(Ghost ghost, Map map) {

        int i;
        for (i = 0; i < map.getN_BLOCKS() * map.getN_BLOCKS(); i++) {
            map.setScreenData(i, map.getLevelData()[i]);
        }
        continueLevel(ghost, map);
    }

    public void gambar() {
        down = loadImages("gambar/down.gif");
        up = loadImages("gambar/up.gif");
        heart = loadImages("gambar/heart.png");
        left = loadImages("gambar/left.gif");
        right = loadImages("gambar/right.gif");
    }

    public void movePacman(Map map) {
        int pos; // untuk mengtahui posisi pacman dalam piksel
        int ch;
        // fungsi untuk mengecek apakah sebuah movement dari satu block ke block lain
        // sudah selesai
        if (pacman_x % map.getBLOCK_SIZE() == 0 && pacman_y % map.getBLOCK_SIZE() == 0) {
            // Mendapatkan lokasi pacman dari screendata
            pos = pacman_x / map.getBLOCK_SIZE() + map.getN_BLOCKS() * (int) (pacman_y / map.getBLOCK_SIZE());
            // Setelah didapatkan lokasinya maka data di assign ke pos
            ch = map.getScreenData()[pos];

            // Untuk mengecek, jik pacman berada pada titik yang bernilai 16 maka score akan
            // ditambah
            if ((ch & 16) != 0) {
                eatSound();
                map.setScreenData(pos, (int) (ch & 15));
                score++;
            }
            // Fungsi untuk mengontorl pacman
            // Jika inputan request x dan y tidak bernilai 0 berarti ada inputan key dari
            // user
            if (req_dx != 0 || req_dy != 0) {
                // untuk mengcek, jika semua kondisi dibawah ini false maka pacman dapat jalan,
                // jika tidak maka tidak boleh jalan karena fungsi di bawah ini tidak akan
                // dieksekusi jika tidak memenuhi syaryat berikut
                // jika pacman berada pada left border (1) maka pacman tidak akan bisa bergerak
                // ke arah kiri
                if (!((req_dx == -1 && req_dy == 0 && (ch & 1) != 0)
                        // jika pacman berada pada right border (4) maka pacman tidak akan bisa bergerak
                        // ke arah kanan
                        || (req_dx == 1 && req_dy == 0 && (ch & 4) != 0)
                        // jika pacman berada pada left border (4) maka pacman tidak akan bisa bergerak
                        // ke arah kanan
                        || (req_dx == 0 && req_dy == -1 && (ch & 2) != 0)
                        // jika pacman berada pada bottom border (8) maka pacman tidak akan bisa
                        // bergerak ke arah kanan
                        || (req_dx == 0 && req_dy == 1 && (ch & 8) != 0))) {
                    // jika pacman tidak berada di border maka dapat digerakkan sesuai request yang
                    // masuk
                    pacmand_x = req_dx;
                    pacmand_y = req_dy;
                }
            }

            // Check for standstill
            // Pacman akan berhenti jika tidak bisa bergerak maju
            if ((pacmand_x == -1 && pacmand_y == 0 && (ch & 1) != 0)
                    || (pacmand_x == 1 && pacmand_y == 0 && (ch & 4) != 0)
                    || (pacmand_x == 0 && pacmand_y == -1 && (ch & 2) != 0)
                    || (pacmand_x == 0 && pacmand_y == 1 && (ch & 8) != 0)) {
                pacmand_x = 0;
                pacmand_y = 0;
            }
        }
        // mengupdate poisisi pacman
        pacman_x = pacman_x + PACMAN_SPEED * pacmand_x;
        pacman_y = pacman_y + PACMAN_SPEED * pacmand_y;
    }

    // untuk menggambar gambar pacman
    public void drawPacman(Graphics2D g2d) {

        if (req_dx == -1) {
            g2d.drawImage(left, pacman_x + 1, pacman_y + 1, this);
        } else if (req_dx == 1) {
            g2d.drawImage(right, pacman_x + 1, pacman_y + 1, this);
        } else if (req_dy == -1) {
            g2d.drawImage(up, pacman_x + 1, pacman_y + 1, this);
        } else {
            g2d.drawImage(down, pacman_x + 1, pacman_y + 1, this);
        }
    }

    //fungsi yang dijalankan ketika pacman mati
    public void death(Ghost ghost, Map map) {
        lives--;
        deadSound();
        //jika pacman mati  dan score lebih tinggi maka di reset
        if (score > highsScore) {
            Crud db = new Crud();
            try {
                //mengupate high score
                db.updateHs(score);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //memanggil hs
        getHs();
        //jika lives 0 maka set game false
        if (lives == 0) {
            //kalo false maka game akan berhenti
            Entities.inGame = false;
            //set score ke 0 lagi
            score = 0;
            //memberhentikan music
            music.start();

        }
        //melanjutkan level
        continueLevel(ghost, map);

    }

    private void continueLevel(Ghost ghost, Map map) {

        int dx = 1;
        int random;// Untuk mengenerate random speed pada ghost

        for (int i = 0; i < ghost.getN_GHOSTS(); i++) {
            // Menetukan posisi awal dari ghost
            ghost.setGhost_y(i, 4 * map.getBLOCK_SIZE());
            ghost.setGhost_x(i, 4 * map.getBLOCK_SIZE());
            ghost.setGhost_dy(i, 0);
            ghost.setGhost_dx(i, dx);
            dx = -dx;
            // Merandom speed ghost
            random = (int) (Math.random() * (getCurrentSpeed() + 1));
            // Jika hasil random lewat maka akan di set sesuai speed yang sekarang
            if (random > getCurrentSpeed()) {
                random = getCurrentSpeed();
            }
            // Speed hanya akan diterima apabila berada dalam valid speed yang ditetapkan
            ghost.setGhostSpeed(i, getValidSpeeds()[random]);
        }
        // Untuk menset poisi awal dari pacman
        pacman_x = 7 * map.getBLOCK_SIZE();
        pacman_y = 11 * map.getBLOCK_SIZE();
        pacmand_x = 0; // reset direction move
        pacmand_y = 0;
        req_dx = 0; // reset direction controls
        req_dy = 0;
        dying = false;

    }

    //getter and setter saja
    public boolean isDying() {
        return this.dying;
    }
    
    public void setDying(boolean dying) {
        this.dying = dying;
    }

    public int getLives() {
        return this.lives;
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int score) {
        this.score = score;
    }


    public BufferedImage getHeart() {
        return this.heart;
    }


    public int getPacman_x() {
        return this.pacman_x;
    }

    public int getPacman_y() {
        return this.pacman_y;
    }

    public void setReq_dx(int req_dx) {
        this.req_dx = req_dx;
    }

    public void setReq_dy(int req_dy) {
        this.req_dy = req_dy;
    }

    Thread music = new Thread(this);
    @Override
    public void run() {
        playSound();
        
    }
}
