
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class Sound {
    Clip clip;
    URL soundURL[] = new URL[10];
    FloatControl fc;

    Sound() {
        soundURL[0] = getClass().getResource("/sounds/startsound.wav");
        soundURL[1] = getClass().getResource("/sounds/playsound.wav");
        soundURL[2] = getClass().getResource("/sounds/mati.wav");
        soundURL[3] = getClass().getResource("/sounds/eatDot.wav");
    }

    public void setFile(int i) {
        try {
            AudioInputStream ais = AudioSystem.getAudioInputStream(soundURL[i]);
            clip = AudioSystem.getClip();
            clip.open(ais);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public Clip getFile(int i){
        try {
            AudioInputStream ais = AudioSystem.getAudioInputStream(soundURL[i]);
            clip = AudioSystem.getClip();
            clip.open(ais);
        } catch (Exception e) {
            e.getStackTrace();
        }
        return clip;
    }

    public void play() {
        // System.out.println(clip);
        clip.start();
    }

    public Clip  getloop() {
        System.out.println(clip);
        clip.loop(Clip.LOOP_CONTINUOUSLY);
        return clip;
    }

    public void stop() {
        clip.stop();
        // System.out.println("stopkah?");
    }
}
