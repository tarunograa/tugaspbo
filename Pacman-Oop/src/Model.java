import java.awt.*;
import javax.swing.JPanel;

public class Model extends JPanel implements Runnable {
    private Dimension d;
    Pacman pacman = new Pacman();
    Map map = new Map();
    Ghost ghost = new Ghost();
    TAdapter adapter;
    Thread thread = new Thread(this);

    public Thread getThread() {
        return this.thread;
    }

    // constructor yang otomatis akan menjalankan beberapa function
    public Model() {
        // instansiasi adapter
        adapter = new TAdapter(pacman, this, map, ghost);
        Entities entities = new Entities();
        // menjalankan sound awal game
        entities.startSound();
        // memanggil hs
        entities.getHs();
        // load gambar
        pacman.gambar();
        ghost.gambar();
        // inisialisasi dimension
        initVariables();
        // inisialisasi variabel ghost
        ghost.initVariables();
        // menjalankan adapter agar dapat menerima input dari keyboard
        addKeyListener(adapter);
        // menjalankan agar frame dapat diselect
        setFocusable(true);
        // init game pacman
        pacman.initGame(ghost, map);
        // start thread
        thread.start();
    }

    // set dimension
    public void initVariables() {
        d = new Dimension(400, 400);
    }

    // fungsi memainkan game
    public void playGame(Graphics2D g2d) {
        if (pacman.isDying()) {
            // jik pacman mati maka ya mati
            pacman.death(ghost, map);

            // pacman.insertScore();

        } else {
            // function utama dalam program yang akan dijalankan
            pacman.movePacman(map);
            pacman.drawPacman(g2d);
            ghost.moveGhosts(g2d, map, pacman);
            map.checkMaze(ghost, pacman);
        }
    }

    // paint component
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.black);
        g2d.fillRect(0, 0, d.width, d.height);

        // menggambar peta game
        map.drawMaze(g2d);
        // draw score ke layar
        map.drawScore(g2d, pacman.getScore(), pacman.getHeart(), pacman.getLives());

        if (Entities.inGame) {
            // System.out.println(Entities.inGame);
            // menjalankan game
            playGame(g2d);
        } else {
            // menampilkan intro screen
            map.showIntroScreen(g2d);
        }

        Toolkit.getDefaultToolkit().sync();
        g2d.dispose();
    }

    // method run agar bisa dijalankan menggunakan thread
    @Override
    public void run() {
        double drawInterval = 1000000000 / 22; // 1 000 000 000 == 1 second == 60 Frames Per Second
        double delta = 0;
        long lastTime = System.nanoTime();
        long currentTime;

        while (thread != null) {
            currentTime = System.nanoTime();
            delta += (currentTime - lastTime) / drawInterval;
            lastTime = currentTime;
            if (delta >= 1) {
                // menjalankan agar repaint terus dieksekusi dimana repaint akan menjalankan
                // paintcompoenent
                repaint();
                delta--;
            }
        }

    }

}
