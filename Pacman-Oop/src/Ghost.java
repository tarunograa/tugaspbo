import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;

public class Ghost extends Entities {
    private final int MAX_GHOSTS = 12;
    private int N_GHOSTS = 6;

    private int[] dx, dy; // variabel yang berfungsi untuk menerima perubahan dari ghost
    private int[] ghost_x, ghost_y;// Variabel yang mengetahui posisi ghost secara vertikal dan horizontal
    private int[] ghost_dx, ghost_dy;// variabl yang akan mempengaruhi perubahan gerakan dari ghost
    private int[] ghostSpeed; // Ghost speed
    private BufferedImage ghost; // Menerima inputan image ghost

    public int getMAX_GHOSTS() {
        return this.MAX_GHOSTS;
    }

    public int getN_GHOSTS() {
        return this.N_GHOSTS;
    }

    public void setN_GHOSTS(int N_GHOSTS) {
        this.N_GHOSTS = N_GHOSTS;
    }

    //menginisialisasi varibel array sejumlah ghost yang ada
    public void initVariables() {
        this.ghost_x = new int[MAX_GHOSTS];
        this.ghost_dx = new int[MAX_GHOSTS];
        this.ghost_y = new int[MAX_GHOSTS];
        this.ghost_dy = new int[MAX_GHOSTS];
        this.ghostSpeed = new int[MAX_GHOSTS];
        this.dx = new int[4];
        this.dy = new int[4];
    }

    //me load gambar ghost
    public void gambar() {
        try {
            ghost = ImageIO.read(getClass().getResource("gambar/ghost.gif"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //fungsi untuk menggerakkan ghost
    public void moveGhosts(Graphics2D g2d, Map map, Pacman pacman) {
        //mengetahui posisi ghost
        int pos;
        int count;

        for (int i = 0; i < N_GHOSTS; i++) {
            //ghost akan bergerak ke block selanjutnya apabila sudah selesai bergerak dari blok satu ke blok lainnya
            if (ghost_x[i] % map.getBLOCK_SIZE() == 0 && ghost_y[i] % map.getBLOCK_SIZE() == 0) {
                //line ini berfungsi untuk mengetahui dimana ghost berada, secara teoritis terdapat 225 posisi yang ada
                pos = ghost_x[i] / map.getBLOCK_SIZE() + map.getN_BLOCKS() * (int) (ghost_y[i] / map.getBLOCK_SIZE());
                count = 0;
                // Jika tidak terdapat rintangan pada sisi kiri dan ghost tidak bergerak ke kanan 
                // maka Ghost akan bergerak ke kiri
                if ((map.getScreenData()[pos] & 1) == 0 && ghost_dx[i] != 1) {
                    dx[count] = -1;
                    dy[count] = 0;
                    count++;
                }
                // Jika tidak terdapat rintangan pada sisi atas dan ghost tidak bergerak ke bawah
                // maka Ghost akan bergerak ke atas
                if ((map.getScreenData()[pos] & 2) == 0 && ghost_dy[i] != 1) {
                    dx[count] = 0;
                    dy[count] = -1;
                    count++;
                }
                // Jika tidak terdapat rintangan pada sisi kanan dan ghost tidak bergerak ke kiri
                // maka Ghost akan bergerak ke kanan
                if ((map.getScreenData()[pos] & 4) == 0 && ghost_dx[i] != -1) {
                    dx[count] = 1;
                    dy[count] = 0;
                    count++;
                }
                // Jika tidak terdapat rintangan pada sisi bawah dan ghost tidak bergerak ke atas
                // maka Ghost akan bergerak ke bawah
                if ((map.getScreenData()[pos] & 8) == 0 && ghost_dy[i] != -1) {
                    dx[count] = 0;
                    dy[count] = 1;
                    count++;
                }
                //meresent count untuk iterasi selanjutnya
                if (count == 0) {
                    System.out.println("kapan ke sini");
                    if (map.getScreenData()[pos] == 15) {
                        ghost_dx[i] = 0;
                        ghost_dy[i] = 0;
                    } else {
                        ghost_dx[i] = -ghost_dx[i];//menggerakkan ghost secar arah berlawanan dx
                        ghost_dy[i] = -ghost_dy[i];//menggerakkan ghost secara arah berlawanan dy
                    }
                    

                } else {
                    //untuk merandom poisis pacman yang akan dijalankan 
                    count = (int) (Math.random() * count);
                    //jika count lebih dari 3, maka reset lagi ke 3
                    //jika tidak di reset ada kemungkinan index array out of bounds
                    if (count > 3) {
                        count = 3;
                    }
                    //merubah arah ghost berdasarkan input dari dx[count]
                    ghost_dx[i] = dx[count];
                    ghost_dy[i] = dy[count];
                }

            }
            // Mengubah poisis gambar ghost
            ghost_x[i] = ghost_x[i] + (ghost_dx[i] * ghostSpeed[i]);
            ghost_y[i] = ghost_y[i] + (ghost_dy[i] * ghostSpeed[i]);
            // mendraw ghost ke layar
            drawGhost(g2d, ghost_x[i] + 1, ghost_y[i] + 1);
            // Mengecek, jika pacman bertabrakan dengan ghost maka pacman akan mati
            //pacman mati apabila posisi x atau y pacman berdekatan setidaknya 1 piksel secara horizontal atau vertikal
            //dan secara horizontal atau vertikal memiliki nilai yang sama
            //Pacman akan mati apabila menyentuh ghost, bukan berarti harus benar benar berada pada titik piksel yang sama
            //pada fungsi ini dilakkan pengecekan jika secara vertikal pacman dan ghost memiliki posisi yang sama maka code ini akan true pacman.getPacman_y() > (ghost_y[i] - 12) && pacman.getPacman_y() < (ghost_y[i] + 12
            //Jika berada pada titik horizontal yang sma maka code ini akan true pacman.getPacman_x() > (ghost_x[i] - 12) && pacman.getPacman_x() < (ghost_x[i] + 12)

            //Sebagai contoh jika pacman berada pada x=0 dan y = 162, dan ghost berada pada titik  x=0 dan y=152 maka pacman akan mati karena mereka bersentuhan secara vertikal dan berjarak hanya 162-152= 10 piksel
            if (pacman.getPacman_x() > (ghost_x[i] - 12) && pacman.getPacman_x() < (ghost_x[i] + 12)
                    && pacman.getPacman_y() > (ghost_y[i] - 12) && pacman.getPacman_y() < (ghost_y[i] + 12)
                    && inGame) {
                // System.out.println(pacman.getPacman_x());
                // System.out.println(ghost_x[i]);
                // System.out.println(pacman.getPacman_y());
                // System.out.println(ghost_y[i]);
                pacman.setDying(true);
                
            }
        }
    }

    private void drawGhost(Graphics2D g2d, int x, int y) {
        g2d.drawImage(ghost, x, y, this);
    }


    public void setGhost_x(int index, int value) {
        this.ghost_x[index] = value;
    }


    public void setGhost_y(int index, int value) {
        this.ghost_y[index] = value;
    }


    public void setGhost_dx(int index, int value) {
        this.ghost_dx[index] = value;
    }


    public void setGhost_dy(int index, int value) {
        this.ghost_dy[index] = value;
    }

    public void setGhostSpeed(int index, int value) {
        this.ghostSpeed[index] = value;
    }

}
