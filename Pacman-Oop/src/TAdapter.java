import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


public class TAdapter extends KeyAdapter implements Runnable{
    // Entities entity;
    Pacman pacman;
    Model model;
    Map map;
    Ghost ghost;
    
    Entities entiti = new Entities();
    public TAdapter(Pacman pacman, Model model, Map map, Ghost ghost) {
        this.pacman = pacman;
        this.model = model;
        this.map = map;
        this.ghost = ghost;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        if (Entities.inGame) {
            if (key == KeyEvent.VK_LEFT) {
                //Left berarti ke kanan sehingga sumbu x -1 dan y tidak dirubah, begitu juga yang lain
                pacman.setReq_dx(-1);
                pacman.setReq_dy(0);
            } else if (key == KeyEvent.VK_RIGHT) {
                pacman.setReq_dx(1);
                pacman.setReq_dy(0);
            } else if (key == KeyEvent.VK_UP) {
                pacman.setReq_dx(0);
                pacman.setReq_dy(-1);
            } else if (key == KeyEvent.VK_DOWN) {
                pacman.setReq_dx(0);
                pacman.setReq_dy(1);
            } else if (key == KeyEvent.VK_ESCAPE && model.getThread()!=null) {
                // sound.stop();
                Entities.inGame = false;

            }
            // sound.stop();

        } else {
            if (key == KeyEvent.VK_SPACE) {
                Entities.inGame = true;
                pacman.initGame(ghost, map);
                entiti.playSound();
                playMusic.start();

            }
        }
    }
    Thread playMusic = new Thread(this);
    @Override
    public void run() {
        entiti.playSound();
    }
    
}
