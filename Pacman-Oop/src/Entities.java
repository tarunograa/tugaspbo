
import javax.swing.JPanel;
import java.awt.image.BufferedImage;
import java.sql.ResultSet;

import javax.imageio.ImageIO;
import javax.sound.sampled.Clip;
public class Entities extends JPanel{
    //Menyimpan informasi game sudah dimainkan apa belum
    public static boolean inGame = false;
    //Variable menyimpan hs
    public static int highsScore;
    //Deklarasi Variabel class sound
    private static Sound sound;
    
    //Constrcutor sound
    public Entities(){
        Entities.sound = new Sound();
    }
    //Valid speed yang diizinkan hanyalah faktor 24
    private int[] validSpeeds = { 1, 2, 3, 4, 6, 8 }; 
    public int[] getValidSpeeds() {
        return this.validSpeeds;
    }

    public void setValidSpeeds(int[] validSpeeds) {
        this.validSpeeds = validSpeeds;
    }

    
    private final int maxSpeed = 6;
    public int getMaxSpeed() {
        return this.maxSpeed;
    }

    private static int currentSpeed = 3;
    public int getCurrentSpeed() {
        return Entities.currentSpeed;
    }

    public void setCurrentSpeed(int currentSpeed) {
        Entities.currentSpeed = currentSpeed;
    }

    //method untuk load images
    public BufferedImage loadImages (String path){
        BufferedImage img=null;
        try {
            img = ImageIO.read(getClass().getResource(path));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return img;
    }

    //Method untuk query highscore
    public void getHs(){
        Crud db = new Crud();
        try {
            ResultSet rs = db.show();
            while(rs.next()){
                Entities.highsScore = rs.getInt("score");
                // System.out.println(hs);
            } 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Method untuk memainkan suara opening dari pacman
    public void startSound(){
        sound.setFile(0);
        sound.play();
    }

    //Method untuk memainkan suara ketika pacman sedang makan 
    public void eatSound(){
        sound.setFile(3);
        sound.play();
    }

    //Deklarasi variabel clip
    static Clip clip;
    //Method untuk memainkan suara utama game yang ngiung ngiung
    public void playSound(){
        //jika in game true
        if(inGame){
            clip = sound.getFile(1);
            //suara utama game akan dimainkan
            Entities.clip.start();
            //Suara utama game akan di loop
            Entities.clip.loop(Clip.LOOP_CONTINUOUSLY);
        }else{
            //Jika ingame false maka suara akan di stop
            Entities.clip.stop();
        }
    }

    //suara ketika pacman mati
    public void deadSound(){
        sound.setFile(2);
        sound.play();
    }
}
