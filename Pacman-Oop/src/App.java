import javax.swing.JFrame;

public class App extends JFrame{
	//Menjalankan Program 
	public App() {
		Model model = new Model();
		add(model);
	}
	
	
	public static void main(String[] args) {
		App app = new App();
		//Set visibility window
		app.setVisible(true);
		//Nama frame window
		app.setTitle("pacman");
		//Ukuran window
		app.setSize(380,420);
		app.setDefaultCloseOperation(EXIT_ON_CLOSE);
		app.setLocationRelativeTo(null);
	}

}
