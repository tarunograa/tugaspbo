public class Driver {
    public static void main(String[] args) throws Exception {
        System.out.println("\n-----Tes Class Bentuk------");
        Bentuk bentuk1 = new Bentuk("Merah");
        bentuk1.printInfo();
        bentuk1.setWarna("Kuning");
        bentuk1.printInfo();
        System.out.println("\n-----Latihan 1------");
        BujurSangkar bujurSangkar = new BujurSangkar(5, "Hitam");
        System.out.println("Panjang Sisi Bujur Sangkar : "+bujurSangkar.getSisi());
        bujurSangkar.printInfo();
        bujurSangkar.setSisi(6);
        System.out.println("Panjang Sisi Bujur Sangkar diubah ke : "+bujurSangkar.getSisi());
        bujurSangkar.printInfo();
        System.out.println("\n-----Latihan 2------");
        Lingkaran lingkaran = new Lingkaran(5, "Hitam");
        System.out.println("Panjang radius Lingkaran : "+lingkaran.getRadius());
        lingkaran.printInfo();
        lingkaran.setRadius(7);
        System.out.println("Panjang radius Lingkaran diubah ke : "+lingkaran.getRadius());
        lingkaran.printInfo();
        System.out.println("\n-----Latihan 3------");
        Silinder silinder = new Silinder(5, "Hitam",10);
        System.out.println("Panjang radius Silinder : "+silinder.getRadius());
        System.out.println("Tinggi Silinder : "+silinder.getTinggi());
        silinder.printInfo();
        silinder.setRadius(7);
        silinder.setTinggi(12);
        System.out.println("Panjang radius Silinder diubah : "+silinder.getRadius());
        System.out.println("Tinggi Silinder diubah : "+silinder.getTinggi());
        silinder.printInfo();
    }
}
