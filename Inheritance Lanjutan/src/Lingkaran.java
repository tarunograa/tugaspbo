public class Lingkaran extends Bentuk{
    private double radius;
    public Lingkaran(double radius, String warna) {
        super(warna);
        this.radius = radius;
    }
    public double getRadius(){
        return this.radius;
    }
    public void setRadius(double radius){
        this.radius = radius;
    }

    public double hitungLuas(){
        return Math.PI*(this.radius*this.radius);
    }
    public void printInfo(){
        System.out.println("Lingkaran berwarna "+ warna+" luas = "+ hitungLuas());
    }
}
