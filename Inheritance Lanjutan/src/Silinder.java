public class Silinder extends Lingkaran{
    private double tinggi;
    public Silinder(double radius, String warna, double tinggi) {
        super(radius, warna);
        this.tinggi = tinggi;
    }

    public double getTinggi(){
        return this.tinggi;
    }

    public void setTinggi(double tinggi){
        this.tinggi = tinggi;
    }

    public double hitungVolume(){
        return hitungLuas()*this.tinggi;
    }
    
    public void printInfo(){
        System.out.println("Silinder warna "+warna+" volume = "+hitungVolume());
    }
}
