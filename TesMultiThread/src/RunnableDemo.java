class RunnableDemo implements Runnable {
    private Thread t;
    private String threadName;
    private int a;
    private int b;
    int sleep;
    
    RunnableDemo( String name, int a, int b, int sleep) {
       threadName = name;
       System.out.println("Creating " +  threadName );
       this.a = a;
       this.b = b;
       this.sleep = sleep;
    }
    
    public void run() {
        try {
            Thread.sleep(sleep);
            System.out.println((5+5));
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Thread " +  threadName + " exiting.");
        
    }
    
    public void start () {
       System.out.println("Starting " +  threadName );
       if (t == null) {
          t = new Thread (this, threadName);
          t.start ();
       }
    }
 }
