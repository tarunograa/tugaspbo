public class BangunRuangBeraksi {
    public static void main(String[] args) throws Exception {
        BangunRuang kubus = new BangunRuang(5);
        BangunRuang balok = new BangunRuang();
        BangunRuang balok2 = new BangunRuang(2,4,5);
        BangunRuang bola = new BangunRuang(5);

        balok.setLebar(6);
        balok.setPanjang(7);
        balok.setTinggi(2);

        System.out.println("Luas Kubus dengan sisi "+kubus.getSisi()+" adalah = "+kubus.luasKubus());
        System.out.println("Volume Kubus dengan sisi "+kubus.getSisi()+" adalah = "+kubus.volumeKubus());
        
        System.out.println("Luas balok dengan panjang, lebar dan tinggi berturut-turut "+balok.getPanjang()+","+balok.getLebar()+","+balok.getTinggi()+" adalah = "+balok.luasBalok());
        System.out.println("Volume balok dengan panjang, lebar dan tinggi berturut-turut "+balok.getPanjang()+","+balok.getLebar()+","+balok.getTinggi()+" adalah = "+balok.volumeBalok());
        
        System.out.println("Luas balok2 dengan panjang, lebar dan tinggi berturut-turut "+balok2.getPanjang()+","+balok2.getLebar()+","+balok2.getTinggi()+" adalah = "+balok2.luasBalok());
        System.out.println("Volume balok2 dengan panjang, lebar dan tinggi berturut-turut "+balok2.getPanjang()+","+balok2.getLebar()+","+balok2.getTinggi()+" adalah = "+balok2.volumeBalok());

        System.out.println("Luas Bola dengan jari "+bola.getSisi()+" adalah = "+bola.luasBola());
        System.out.println("Volume Bola dengan jari "+bola.getSisi()+" adalah = "+bola.volumeBola());
        
    }
}
