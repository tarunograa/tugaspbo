public class BangunRuang {
    private int panjang;
    private int lebar;
    private int tinggi;
    private int sisi;

    public BangunRuang(){}
    public BangunRuang(int sisi){
        this.sisi = sisi;
    }

    public BangunRuang(int panjang, int lebar, int tinggi){
        this.panjang = panjang;
        this.lebar = lebar;
        this.tinggi = tinggi;
    }

   
    public int getSisi(){
        return this.sisi;
    }
    public void setSisi(int newSisi){
        this.sisi = newSisi;
    }

    public int getPanjang(){
        return this.panjang;
    }
    public void setPanjang(int newPanjang){
        this.panjang= newPanjang;
    }

    public int getLebar(){
        return this.lebar;
    }
    public void setLebar(int newLebar){
        this.lebar = newLebar;
    }
    
    public int getTinggi(){
        return this.tinggi;
    }
    public void setTinggi(int newTinggi){
        this.tinggi = newTinggi;
    }

    int luasKubus(){
        return 6*(sisi*sisi);
    }
    int luasBalok(){
        return (2*panjang*lebar)+(2*panjang*tinggi)+(2*lebar*tinggi);
    }
   
    int volumeKubus(){
        return sisi*sisi*sisi;
    }
    int volumeBalok(){
        return panjang*tinggi*lebar;
    }

    double luasBola(){
        return 4*Math.PI*(sisi*sisi);
    }
    double volumeBola(){
        return (4*Math.PI*(sisi*sisi*sisi))/3;
    }
        
    
}