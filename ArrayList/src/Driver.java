public class Driver {
    public static void main(String[] args) throws Exception {
        Bank bank = new Bank();
        bank.addCustomer("Taruno", "Graa");
        Account Taruno1 = new Account(50000);
        bank.addCustomer("Rizka", "Islami");
        Account Rizka1 = new Account(50000);
        Account Rizka2 = new Account(50000);
        Account Rizka3 = new Account(50000);
        Account Rizka4 = new Account(50000);
        bank.addCustomer("Baiq", "Aisyah");

        Customer Taruno = bank.getCustomer(0);
        Customer Rizka = bank.getCustomer(1);
        Rizka.setAccount(Rizka1);
        Rizka.setAccount(Rizka2);
        Rizka.setAccount(Rizka3);
        Rizka.setAccount(Rizka4);
        
        Taruno.setAccount(Taruno1);
        Taruno.getAccount(0).deposit(100000);
        System.out.println("jumlah customer bank "+bank.getNumOfCustomers());
        System.out.println("Saldo "+Taruno.getFirstName()+" pada akun ke 0 adalah = "+Taruno.getAccount(0).getBalance());
        System.out.println("Withdraw dari akun Taruno1 "+Taruno.getAccount(0).withdraw(50000));
        System.out.println("Sisa saldo "+Taruno.getFirstName()+" pada akun ke 0 adalah = "+Taruno.getAccount(0).getBalance());
        
        
        System.out.println("jumlah akun Rizka "+Rizka.getNumOfAccounts());
    }
}
