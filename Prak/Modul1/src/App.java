public class App {
    public static void main(String[] args) throws Exception {
        //Membuat Player
        Player uno = new Player("uno");
        Pet kelinci = new Pet("Kelinci");
        Pet kambing = new Pet("Kambing");
        Weapon kapak = new Weapon("Kapak");
        Weapon pedangZeus = new Weapon("Pedang Zeus");
        Monster monster1 = new Monster("Monster1");
        Monster monster2 = new Monster("Monster1");

        //Menangkap pet
        uno.tangkapPet(kambing);
        uno.tangkapPet(kelinci);

        //pickup senjata
        uno.ambilWeapon(kapak);
        uno.gantiWeapon(pedangZeus);
        //serang monster tanpa senjata & pet
        uno.serangMonster(monster1);
        //serang monster menggunakan senjata
        uno.useWeapon();
        uno.serangMonster(monster1);
        uno.UnUseWeapon();
        //serang monster menggunakan pet
        uno.usePet();
        uno.serangMonster(monster1);

        //serang monster menggunakan pet&senjata
        uno.useWeapon();
        uno.serangMonster(monster1);

        //serang sampe levelUp
        while(uno.getLevel()==1 && monster1.getHP()>0){
            uno.serangMonster(monster1);
            
        }
        //serang sampe mati
        System.out.println("Level Uno sekarang :"+uno.getLevel());
        System.out.println("XP Uno sekarang :"+uno.getLevel());
        while(uno.getLevel()==1 && monster2.getHP()>0){
            uno.serangMonster(monster2);
            
        }

    }
}
