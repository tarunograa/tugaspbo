public class Weapon extends Entity{
    //Weapon kdang memiliki user kadang tidak
    private boolean owner = false;

    public boolean isOwner() {
        return this.owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }
    public Weapon(String name) {
        super(name);
    }
    public void downHP(int att){
        setHP(getHP() - att);
    }
    public void gotExp(int exp){
        setExp(getExp()+exp);
    }
    public void dropHp(){
        setHP(getHP()-getBaseAttack());
    }

}
