public class Entity {
    private String name;
    private int level = 1;
    private int baseAttack = this.level * 2;
    private int maxHP = this.level * 10;
    private int HP = this.maxHP;
    private int exp = 0;
    private int maxXP = this.level * 5;

    public Entity(String name){
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getBaseAttack() {
        return this.baseAttack;
    }

    public void setBaseAttack(int baseAttack) {
        this.baseAttack = baseAttack;
    }

    public int getMaxHP() {
        return this.maxHP;
    }

    public void setMaxHP(int maxHP) {
        this.maxHP = maxHP;
    }

    public int getHP() {
        return this.HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getExp() {
        return this.exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getMaxXP() {
        return this.maxXP;
    }

    public void setMaxXP(int maxXP) {
        this.maxXP = maxXP;
    }
    public void gotExp(int exp){
        setExp(getExp()+exp);
        while(getExp()>getMaxXP()){
            setExp(getExp()-getMaxXP());
            level+=1;
        }
    }
}
