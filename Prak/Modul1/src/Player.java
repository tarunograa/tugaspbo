import java.util.Random;
public class Player extends Entity{
    private Random rd = new Random();
    private Weapon senjata;
    private Pet peliharaan;
    private boolean PetCheck = false;
    private boolean WeapCheck = false;

    private boolean usePet = false;
    private boolean useWeap = false;

    public Player(String name){
        super(name);
    }

    //Menggunakan weapon
    //Weapon dapat digunakan player
    public void ambilWeapon(Weapon senjata){
        this.senjata = senjata;
        senjata.setOwner(true);
        WeapCheck = true;
        System.out.println(senjata.getName()+" berhasil di pickup");
        
    }
    //Player menangkap Pet
    public void tangkapPet(Pet pet){
        //PET hanya boleh di tangkap oleh PLAYER ketika tidak memiliki owner lain.
        if(!PetCheck){
            if(this.peliharaan==null){
                this.peliharaan = pet;
                peliharaan.setOwner(true);
                System.out.println("Berhasil menangkap pet bernama "+pet.getName());
                return;
            }
        }
        System.out.println("Gagal menangkap pet bernama : "+pet.getName());
    }

    //Player bisa menyerang Monster
    public void attack (Monster monster, int attSenjata){
        monster.setHP(monster.getHP()-(getBaseAttack()+attSenjata));
        System.out.println("Serangan "+(getBaseAttack()+attSenjata)+" berhasil diberikan");
        //ketika diserang, MONSTER akan melakukan serangan balik terhadap penyerangnya
        //MONSTER tidak melakukaan penyerangaan terhadap apapun yang tidak menyerangnya.
        setHP(monster.serangBalik(getHP()));
        //ketika PLAYER yaang memiliki pet tersebut  diserang, maka PET memiliki peluang 50%
		//untuk melakukan healing terhadap PLAYER tersebut
        if(rd.nextBoolean() && this.peliharaan!=null){
            setHP(getHP()+peliharaan.Heal());   
        }
        if(usePet){
            peliharaan.serangMonster(monster);
        }
    }
   
    public void serangMonster(Monster monster){
        //Serangan ke Monster
        if(useWeap){
            attack(monster, senjata.getBaseAttack());
            //Penurunan hp senjata
            senjata.dropHp();
        }else{
            attack(monster, 0);
        }

        //Kematian peliharaan
        if(peliharaan.getHP()<=0){
            //Pembagian XP dari peliharaan
            //Drop XP mod
            int dropXp = peliharaan.dropXp();
            gotExp(dropXp/2);
            senjata.gotExp(dropXp/2);

            //MONSTER bisa mengalami level_up ketika membunuh PET dari seorang player
            monster.levelUp();
            peliharaan = null;
            PetCheck = false;
            usePet = false;
        }

        //Kematian User
        if(getHP()<=0){
            System.out.println("Game Over");mati();
            return;
            
        }

        //Pembagian XP dari Monster
        if(monster.getHP()<=0){
            int dropXP = monster.dropXp();//Drop XP mod
            System.out.println("nilai dropXP "+monster.dropXp());
            if(WeapCheck && PetCheck){
                //Exp dibagi rata ke 3 entitas
                dropXP = dropXP/3;
                gotExp(dropXP);
                senjata.gotExp(dropXP);
                peliharaan.gotExp(dropXP);

            }else if(WeapCheck){
                //Exp dibagi rata ke 2 entitas (weapon dan user)
                dropXP = dropXP/2;
                gotExp(dropXP);
                senjata.gotExp(dropXP);
            }else if(PetCheck){
                //Exp dibagi rata ke 2 entitas (pet dan user)
                dropXP = dropXP/2;
                gotExp(dropXP);
                peliharaan.gotExp(dropXP);
            }else{
                //Exp hanya didapatkan player
                gotExp(dropXP);
            }
            System.out.println(monster.getName()+" telah mati");
            return;
        }
        
    }

    public void mati(){
        this.senjata.setOwner(false);
        this.peliharaan.setOwner(false);
    }
    
    //Bebas mengganti weapon
    public void gantiWeapon(Weapon senjata){
        dropWeapon(this.senjata);
        ambilWeapon(senjata);
    }
    public void dropWeapon(Weapon senjata){
        WeapCheck = false;
        senjata.setOwner(false);
        System.out.println(senjata.getName()+" di drop");
        
    }
    public void useWeapon(){
        this.useWeap = true;
    }

    public void UnUseWeapon(){
        this.useWeap = false;
    }

    public void usePet(){
        this.usePet = true;
    }

    public void unUsePet(){
        this.usePet = false;
    }


        
}
