public class Pet extends Mob{
    private boolean owner = false;

    public boolean isOwner() {
        return this.owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }
    public Pet(String name) {
        super(name);
    }

    public void serangMonster(Monster monster){
        monster.setHP(monster.getHP()-getBaseAttack());
        //ketika diserang, MONSTER akan melakukan serangan balik terhadap penyerangnya
        System.out.println("Serangan pet "+(getBaseAttack())+" berhasil diberikan");
        System.out.print("pet ");
        setHP(monster.serangBalik(getHP()));
    }
    public int Heal(){
        int heal = getLevel()*2;
        System.out.println("Healing sebanyak "+heal+" berhasil diberikan");
        return heal;
    }
    
}
