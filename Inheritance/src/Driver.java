public class Driver {
    public static void main(String[] args) throws Exception {
        SegiEmpat persegi = new SegiEmpat();
        Balok balok = new Balok();
        persegi.setLebar(4);
        persegi.setPanjang(8);
        
        balok.setTinggi(6);
        balok.setLebar(4);
        balok.setPanjang(8);
        System.out.println("Luas Persegi : "+persegi.luas());
        System.out.println("Volume Balok : "+balok.volume());
    }
}

