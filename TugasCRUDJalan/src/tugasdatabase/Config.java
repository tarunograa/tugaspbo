/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasdatabase;

//import static crud.dasar.CrudDasar.JDBC_DRIVER;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Taruno Graa
 */
public class Config {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/tugaspbo";
    static final String USER = "root";
    static final String PASS = "";

    // Menyiapkan objek yang diperlukan untuk mengelola database
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;

    public Config(){
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void show(){
        String sql = "SELECT * FROM mahasiswa";
            
            // eksekusi query dan simpan hasilnya di obj ResultSet
            try{
               rs = stmt.executeQuery(sql);
            
            // tampilkan hasil query
            while(rs.next()){
                System.out.println("NIM: " + rs.getString("nim"));
                System.out.println("Nama: " + rs.getString("nama"));
                System.out.println("Jurusan: " + rs.getString("jurusan"));
                System.out.println("Jenis Kelamin: " + rs.getString("gender"));
            } 
            }catch (Exception e){
                e.printStackTrace();
            }   
    }
    
     public void execute(String data){
        try{
            stmt.execute(data);
        }catch(Exception e){
            e.printStackTrace();
        }
     }
     
 
            
}
